import Vue from 'vue'
import vuetify from './plugins/vuetify';
import { router } from './router';
import { ValidationProvider } from 'vee-validate';

// Register it globally
// main.js or any entry file.
Vue.component('ValidationProvider', ValidationProvider);
Vue.config.productionTip = false

new Vue({
  router,
  vuetify,
  render: h => h( { template: '<router-view></router-view>' } )
}).$mount('#app')
