import Vue from 'vue';
import VueRouter from 'vue-router';
import Layout from './components/Layout'
import Form from './components/Form'
import App1 from './components/App1'
import AppButtons1 from './components/AppButtons1'
import App2 from './components/App2'
import AppButtons2 from './components/AppButtons2'

Vue.use( VueRouter )

let routes = [
    { 
        path: '/', 
        component: Layout,
        children: [
            {   
                name: 'form',
                path: 'form', 
                components: {
                    default: Form,
                    buttons: {template: '<router-view name="buttons"></router-view>'}
                },
                children: [
                    {
                        name: 'app1', 
                        path: 'app1', 
                        components: {
                            default: App1,
                            buttons: AppButtons1
                        },
                    },
                    {
                        name: 'app2', 
                        path: 'app2', 
                        components: {
                            default: App2,
                            buttons: AppButtons2
                        },
                    }
                ]
            }
        ]
    }
]


export let router = new VueRouter({
    routes
});